//
//  KSQueue.swift
//  
//
//  Created by Keith Staines on 22/01/2021.
//

/// A KSQueue is a first-in, first out store. New items are added to the queue by `enqueue`, which adds the Item at the queue's current last position. Items are removed from the queue by `dequeue`, which removes the item at the front of the queue
public struct KSQueue<Element> {
    /// A type representing the queued elements
    public typealias Element = Element
    
    /// True if the queue is empty, otherwise false
    public var isEmpty: Bool { _store.isEmpty }
    
    /// The number of elements stored in the queue
    public var count: Int { _store.count }
    
    private var _store: [Element] = []
    
    /// Adds an element to the queue
    ///
    /// - Parameter item: The item to be added to the queue. The item will join at the end of the queue
    public mutating func enqueue(item: Element) {
        _store.insert(item, at: 0)
    }
    
    /// Removes and returns the element that is at the front of the queue
    ///
    /// - Returns: The item that is currently at the front of the queue, or nil if the queue is empty
    public mutating func dequeue() -> Element? {
        isEmpty ? nil : _store.removeLast()
    }
    
    /// Initialises an empty queue
    public init() {
    }
    
    /// Initialising with a sequence.
    ///
    /// Initialising with `[1,2,3]` is equivalent to
    ///
    ///     var queue = KSQueue<Int>()
    ///     queue.enqueue(1)
    ///     queue.enqueue(2)
    ///     queue.enqueue(3)
    /// - Parameter elements: The elements that will be enqueued in order
    public init<S>(_ elements: S) where S : Sequence, Self.Element == S.Element {
        _store = Array(elements).reversed()
    }
}
