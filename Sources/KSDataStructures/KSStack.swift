//
//  KSStack.swift
//  
//
//  Created by Keith Staines on 23/01/2021.
//

/// Implements a first-in, last-out store
public struct KSStack<Element> {
    /// A type representing the stacked elements
    public typealias Element = Element
    
    /// True if the stack is empty, otherwise false
    public var isEmpty: Bool { _store.isEmpty }
    
    /// Returns the number of elements in the stack
    public var count: Int { _store.count }
    
    private var _store: [Element] = []
    
    /// Adds an element to the top of the stack
    ///
    /// - Parameter item: The item to be added to the top of the stack.
    public mutating func push(_ item: Element) {
        _store.append(item)
    }
    
    /// Allows the element currently at the top of the stack to be inspected without removing it
    ///
    /// - Returns: The element at the top of the stack, or nil if the stack is empty
    public func peek() -> Element? {
        _store.last
    }
    
    /// Removes and returns the element at the top of the stack
    ///
    /// - Returns: The element at the top of the stack, or nil if the stack is empty
    public mutating func pop() -> Element? {
        isEmpty ?  nil : _store.removeLast()
    }
    
    /// Initialises an empty stack
    public init() {
    }
    
    /// Initialises a stack with the contents of a sequence
    ///
    /// Initialising with `[1,2,3]` is equivalent to initialising empty, followed by
    /// 
    ///     push(1)
    ///     push(2)
    ///     push(3)
    ///
    /// - Parameter elements: A sequence containing the elements to be added to the stack
    public init<S>(_ elements: S) where S : Sequence, Self.Element == S.Element {
        _store = Array(elements)
    }
}
