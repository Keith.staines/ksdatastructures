//
//  KSStackTests.swift
//  
//
//  Created by Keith Staines on 23/01/2021.
//

import XCTest
import KSDataStructures

final class KSStackTests: XCTestCase {
    
    func test_init_empty() {
        let sut = KSStack<String>()
        XCTAssertTrue(sut.isEmpty)
        XCTAssertEqual(sut.count, 0)
    }
    
    func test_init_with_sequence() {
        var sut = KSStack([1,2,3])
        XCTAssertFalse(sut.isEmpty)
        XCTAssertEqual(sut.count, 3)
        XCTAssertEqual(sut.peek(), 3)
        XCTAssertEqual(sut.pop(), 3)
        XCTAssertEqual(sut.pop(), 2)
        XCTAssertEqual(sut.pop(), 1)
        XCTAssertNil(sut.pop())
        XCTAssertTrue(sut.isEmpty)
        XCTAssertEqual(sut.count, 0)
    }
    
    func test_push() {
        var sut = KSStack<String>()
        sut.push("hello")
        XCTAssertFalse(sut.isEmpty)
        XCTAssertEqual(sut.count, 1)
        XCTAssertTrue(sut.peek() == "hello")
    }
    
    func test_peek() {
        var sut = KSStack([1,2,3])
        sut.push(4)
        XCTAssertTrue(sut.peek() == 4)
    }
    
    func test_pop() {
        var sut = KSStack([1,2,3])
        let popped = sut.pop()
        XCTAssertEqual(popped, 3)
        XCTAssertTrue(sut.peek() == 2)
    }
    
    
}
