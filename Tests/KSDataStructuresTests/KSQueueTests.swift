import XCTest
@testable import KSDataStructures

final class KSQueueTests: XCTestCase {
    
    func test_empty_queue() {
        let sut = KSQueue<String>()
        XCTAssertTrue(sut.isEmpty)
    }
    
    func test_enque_dequeue() {
        var sut = KSQueue<Int>()
        sut.enqueue(item: 0)
        sut.enqueue(item: 1)
        sut.enqueue(item: 2)
        XCTAssertFalse(sut.isEmpty)
        XCTAssertEqual(sut.dequeue(), 0)
        XCTAssertEqual(sut.dequeue(), 1)
        XCTAssertEqual(sut.dequeue(), 2)
        XCTAssertNil(sut.dequeue())
        XCTAssertTrue(sut.isEmpty)
        XCTAssertEqual(sut.count, 0)
    }
    
    func test_enqueue_dequeue_with_sequence() {
        var sut = KSQueue.init([0,1,2])
        sut.enqueue(item: 3)
        XCTAssertFalse(sut.isEmpty)
        XCTAssertEqual(sut.count, 4)
        XCTAssertEqual(sut.dequeue(), 0)
        XCTAssertEqual(sut.dequeue(), 1)
        XCTAssertEqual(sut.dequeue(), 2)
        XCTAssertEqual(sut.dequeue(), 3)
        XCTAssertNil(sut.dequeue())
        XCTAssertTrue(sut.isEmpty)
        XCTAssertEqual(sut.count, 0)
    }

}
